package domaine;

/**
 * Module 634.1 - TP Série P03
 *
 * Entité Facture
 * 
 * @author Jonathan Blum
 */
public class Facture {

  private int noFacture;
  private Client client;
  private String description;
  private int montant;

  /** Constructeurs */
  public Facture (int noFacture, Client client, String description, int montant) {
    this.noFacture = noFacture;
    this.client = client;
    this.description = description;
    this.montant = montant;
  } // Constructeur

  public Facture (Client client, String description, int montant) {this(0, client, description, montant);}

  public int getNoFacture () {return noFacture;}
  public Client getClient () {return client;}
  public String getDescription() {return description;}
  public int getMontant () {return montant;}

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + this.noFacture;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Facture other = (Facture) obj;
        if (this.noFacture != other.noFacture) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.description + " - CHF " + this.montant;
    }


  
} // Facture
