package domaine;

/**
 * Module 634.1 - TP Série P03
 *
 * Entité Client
 *
 * @author Jonathan Blum
 */
public class Client {

  private int noClient;
  private String nom;
  private String prenom;

  /** Constructeur */
  public Client (int noClient, String nom, String prenom) {
    this.noClient = noClient;
    this.nom = nom;
    this.prenom = prenom;
  } // Constructeur

  public int getNoClient () {return noClient;}
  public String getNom () {return nom;}
  public String getPrenom () {return prenom;}

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.noClient;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (this.noClient != other.noClient) {
            return false;
        }
        return true;
    }
  
    @Override
    public String toString() {
        return this.nom + " " + this.prenom;
    } 
  
} // Client