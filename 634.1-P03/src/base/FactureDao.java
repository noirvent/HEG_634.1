package base;

import domaine.Client;
import domaine.Facture;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Module 634.1 - TP Série P03
 *
 * Gestion des accès à la base de données pour l'entité Facture.
 * 
 * @author Jonathan Blum
 */
public class FactureDao {

  /** Retourne la liste des facture du Client c, dans l'ordre des numéros de facture. */
  public static ArrayList getListeFactures (Client c) {
    /* A COMPLETER */
    ArrayList lst = new ArrayList();
    try {
        Connection con = ConnexionBase.get();
        PreparedStatement stmt = con.prepareStatement(
                "SELECT * "
                + "FROM `Facture` "
                + "WHERE `NoClient` = ? "
                + "ORDER BY `NoFacture`"
        );
        stmt.setInt(1, c.getNoClient());
        
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            lst.add(new Facture(rs.getInt("NoFacture"),
                                c,
                                rs.getString("Description"),
                                rs.getInt("Montant")
            ));      
        }
        stmt.close();
    } catch (SQLException ex) {
        System.err.println("FactureDao.getListeFactures(): " + ex.getMessage());
        return null;
    }
    return lst;
  } // getListeFactures

  /***************************
   * À COMPLETER SI NÉCESSAIRE
   ***************************/
  
  public static int addFacture(Facture f) {
     int id = -1;
     try {
         Connection con = ConnexionBase.get();
         PreparedStatement stmt = con.prepareStatement(
                 "INSERT INTO `Facture` (`NoClient`, `Description`, `Montant`) "
                 + "VALUES (?, ?, ?)",
                 Statement.RETURN_GENERATED_KEYS
         );
         stmt.setInt(1, f.getClient().getNoClient());
         stmt.setString(2, f.getDescription());
         stmt.setInt(3, f.getMontant());
         stmt.executeUpdate();
         ResultSet key = stmt.getGeneratedKeys();
         if(key.next()) {
            id =  key.getInt(1);
         }
         stmt.close();
     } catch(SQLException ex) {
         System.err.println("FactureDao.addFacture(): " + ex.getMessage());
     } 
      return id;
  }
} // FactureDao
