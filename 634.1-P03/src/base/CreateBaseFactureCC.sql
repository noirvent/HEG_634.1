-- Script de création pour MySQL de la base FactureCC
--
-- Peter Daehne, 09.03.2015
-- Encodage UTF-8

--
-- Base de données: FactureCC
--
drop database if exists FactureCC;
create database FactureCC;
use FactureCC;

-- Table Client
drop table if exists Client;
create table Client
(
  NoClient  INT not null auto_increment comment 'Numéro du client (identifiant)',
  Nom       VARCHAR(80)                 comment 'Nom du client',
  Prenom    VARCHAR(80)                 comment 'Prénom du client',
  PRIMARY KEY (NoClient)
);
insert into Client (NoClient, Nom, Prenom) values (1, 'VOUILLAMOZ', 'Laura');
insert into Client (NoClient, Nom, Prenom) values (2, 'CANDOLFI', 'David');
insert into Client (NoClient, Nom, Prenom) values (3, 'FUOCHI', 'Nicolas');
insert into Client (NoClient, Nom, Prenom) values (4, 'ENDT', 'Laetitia');
insert into Client (NoClient, Nom, Prenom) values (5, 'NGUYEN', 'Anaïs');
insert into Client (NoClient, Nom, Prenom) values (6, 'MURISIER', 'Christophe');
insert into Client (NoClient, Nom, Prenom) values (7, 'SCUDERI', 'Mathieu');
insert into Client (NoClient, Nom, Prenom) values (8, 'PAGES', 'Alejandro');
insert into Client (NoClient, Nom, Prenom) values (9, 'WINKELMANN', 'Rebecca');
insert into Client (NoClient, Nom, Prenom) values (10, 'TREMBLET', 'Alexandre');
commit;


-- Table Facture
drop table if exists Facture;
create table Facture
(
  NoFacture    INT not null auto_increment comment 'Numéro de la facture (identifiant)',
  NoClient     INT not null                comment 'Identifiant du client',
  Description  VARCHAR(255)                comment 'Description de l''article',
  Montant      INT not null                comment 'Montant de la facture',
  PRIMARY KEY (NoFacture),
  FOREIGN KEY (NoClient) REFERENCES Client (NoClient)
);
insert into Facture (NoClient, Description, Montant) values (1, 'Boîte de sachets de thé', 4);
insert into Facture (NoClient, Description, Montant) values (1, 'Tasse', 7);
insert into Facture (NoClient, Description, Montant) values (2, 'Lampe de bureau', 52);
insert into Facture (NoClient, Description, Montant) values (2, 'Dictionnaire Français-Anglais', 32);
insert into Facture (NoClient, Description, Montant) values (2, 'Ballon de Football', 45);
insert into Facture (NoClient, Description, Montant) values (3, 'Cahier', 3);
insert into Facture (NoClient, Description, Montant) values (4, 'Stylo', 1);
insert into Facture (NoClient, Description, Montant) values (6, 'Cactus', 6);
insert into Facture (NoClient, Description, Montant) values (6, 'Sac à main', 250);
insert into Facture (NoClient, Description, Montant) values (7, 'Jeu de carte', 2);
insert into Facture (NoClient, Description, Montant) values (8, 'Drap', 15);
insert into Facture (NoClient, Description, Montant) values (8, 'Lecteur DVD', 159);
insert into Facture (NoClient, Description, Montant) values (8, 'T-Shirt', 15);
insert into Facture (NoClient, Description, Montant) values (9, 'Serviette de bain', 16);
insert into Facture (NoClient, Description, Montant) values (10, 'Imprimante', 259);
insert into Facture (NoClient, Description, Montant) values (10, 'Miroir', 43);
commit;