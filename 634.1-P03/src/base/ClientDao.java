package base;

import domaine.Client;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Module 634.1 - TP Série P03
 *
 * Gestion des accès à la base de données pour l'entité Client.
 * 
 * @author Jonathan Blum
 */
public class ClientDao {

  /** Retourne la liste des clients, dans l'ordre des nom et prénom. */
  public static ArrayList getListeClients () {
    /* A COMPLETER */
    ArrayList lst = new ArrayList();
    try {
        Connection con = ConnexionBase.get();
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(
                "SELECT * "
                + "FROM `Client` "
                + "ORDER BY `Nom` "
        );
        while (rs.next()) {
            lst.add(new Client(
                    rs.getInt("NoClient"),
                    rs.getString("Nom"), 
                    rs.getString("Prenom")
            ));      
        }
        stmt.close();
    } catch (SQLException ex) {
        System.err.println("ClientDao.getListeClients(): " + ex.getMessage());
        return null;
    }
    return lst;
  } // getListeClients

  /***************************
   * À COMPLETER SI NÉCESSAIRE
   ***************************/
  
  
} // ClientDao
