package metier;

import base.FactureDao;
import domaine.Client;
import domaine.Facture;
import java.util.Iterator;

/**
 * Module 634.1 - TP Série P03
 * 
 * Liste des factures d'un client avec une position courante
 * 
 * @author Jonathan Blum
 */
public class ListeFactures extends ListeObjects {

  private Client c;

  public ListeFactures (Client c) {
    this.c = c;
    liste = FactureDao.getListeFactures(c);
  } // Constructeur

  /** Retourne la Facture courante, null si la position courante est NO_POS */
  public Facture getCourant () {
    return (Facture) super.getCourant();
  } // getCourant

  /** Retourne la Facture d'indice k, null si k n'est pas correctement défini */
  public Facture get (int k) {
    return (Facture) super.get(k);
  } // get

  /**
   * Retourne le montant total des factures de la liste
   */
  public int getMontantTotal () {
    int sum = 0;
      for (Iterator it = liste.iterator(); it.hasNext();) {
          sum += ((Facture)it.next()).getMontant();
      }
    return sum;
  } // getMontantTotal

  /**
   * Ajoute une facture à la liste de celles du Client c
   */
  public void add (Facture f) {
      int id = FactureDao.addFacture(f);
      f = new Facture(id, f.getClient(), f.getDescription(), f.getMontant());
      liste.add(f);
      setPos(liste.size()-1);
  } // add
  
  /**
   * Ajoute une facture à la liste de celles du Client c
   * Methode d'abstraction à la couche domaine
   */
  public void add(Client cli, String desc, int montant) {
      Facture fact = new Facture(cli, desc, montant);
      add(fact);
  }
  
} // ListeFactures
