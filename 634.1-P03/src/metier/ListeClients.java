package metier;

import base.ClientDao;
import domaine.Client;

/**
 * Module 634.1 - TP Série P03
 *
 * Liste des clients (dans l'ordre des nom et prénom) avec une position courante
 * 
 * @author Jonathan Blum
 */
public class ListeClients extends ListeObjects {

  /** Constructeur */
  public ListeClients () {liste = ClientDao.getListeClients();}

  /** Retourne le client courant, null si la position courante est NO_POS */
  public Client getCourant () {return (Client)super.getCourant();}

  /** Retourne le client d'indice k, null si k n'est pas correctement défini */
  public Client get (int k) {return (Client)super.get(k);}

} // ListeEmployes
