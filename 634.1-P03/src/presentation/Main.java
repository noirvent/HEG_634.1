package presentation;

/**
 * Module 634.1 - TP Série P03
 *
 * Classe principale de l'application
 */
public class Main {

  /** Ouverture de la fenêtre principale de l'application */
  public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new FrmMain().setVisible(true);
      }
    });
  } // main

} // Main
