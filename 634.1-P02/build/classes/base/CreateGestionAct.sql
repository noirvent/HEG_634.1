-- Script de création pour MySQL de la base GestionAct
--
-- Peter DAEHNE, 02.03.2014
-- Encodage UTF-8

--
-- Base de données: GestionAct
--
DROP DATABASE IF EXISTS GestionAct;
CREATE DATABASE GestionAct;
USE GestionAct;

-- Table Participant
DROP TABLE IF EXISTS Participant;
CREATE TABLE Participant
(
  NoPart    INT AUTO_INCREMENT NOT NULL COMMENT 'Numéro du participant (identifiant)',
  Nom       VARCHAR(80)                 COMMENT 'Nom du participant',
  Prenom    VARCHAR(80)                 COMMENT 'Prénom du participant',
  Telephone VARCHAR(20)                 COMMENT 'Téléphone du participant',
  PRIMARY KEY (NoPart)
);
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (1, 'VOUILLAMOZ', 'Laura', '+41.76.798.42.62');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (2, 'CANDOLFI', 'David', '+41.22.757.34.48');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (3, 'FUOCHI', 'Nicolas', '+41.22.348.40.62');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (4, 'ENDT', 'Laetitia', '+41.22.731.19.54');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (5, 'NGUYEN', 'Anaïs', '+41.22.200.18.88');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (6, 'MURISIER', 'Christophe', '+41.22.733.45.42');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (7, 'SCUDERI', 'Mathieu', '+41.22.860.24.14');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (8, 'PAGES', 'Alejandro', '+41.22.738.41.49');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (9, 'WINKELMANN', 'Rebecca', '+41.22.752.14.23');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (10, 'TREMBLET', 'Alexandre', '+41.22.755.15.92');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (11, 'RUFENER', 'Flore', '+41.22.756.04.61');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (12, 'FRIEDLI', 'Ambre', '+41.22.792.62.05');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (13, 'CHRUSCIEL', 'Mikaël', '+41.22.733.53.91');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (14, 'BUSSARD', 'Jérome', '+41.22.349.19.76');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (15, 'TANNER', 'Parik', '+41.22.777.05.91');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (16, 'PASQUIER', 'Allison', '+41.22.774.82.18');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (17, 'ENDT', 'Nicolas', '+41.22.781.16.25');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (18, 'BAEHLER', 'Nathan', '+41.22.600.33.83');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (19, 'ANDREETTA', 'Maëlle', '+41.22.734.63.43');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (20, 'GIROD', 'Kévin', '+41.22.757.10.45');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (21, 'ACKERMANN', 'Camille', '+41.22.354.04.61');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (22, 'SCHALLER', 'Luce', '+41.22.349.55.56');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (23, 'FRAGA', 'Richard', '+41.22.794.93.25');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (24, 'GOLLARZA', 'Edgar', '+41.22.855.15.23');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (25, 'AGOSTINI', 'Margot', '+41.22.733.51.02');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (26, 'MEYLAN', 'Aurélie', '+41.22.343.39.10');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (27, 'MARITZ', 'Laurent', '+41.78.345.69.05');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (28, 'STALDER', 'Juliette', '+41.76.477.06.70');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (29, 'KUNZ', 'Abdulgani', '+41.22.736.30.68');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (30, 'SIMONET', 'Andréas', '+41.22.756.00.25');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (31, 'WRONKOWSKI', 'Timothé', '+41.22.798.70.21');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (32, 'VILA', 'Jonas', '+41.22.781.23.27');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (33, 'PINTO', 'Barbara', '+41.22.740.15.51');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (34, 'LAPLACE', 'Guillaume', '+41.22.752.00.74');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (35, 'RUPPEN', 'Elisa', '+41.22.793.00.30');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (36, 'BUSSARD', 'Julien', '+41.22.771.58.37');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (37, 'DUPLAY', 'Laëtitia', '+41.22.792.07.86');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (38, 'MEYLAN', 'Alexia', '+41.22.793.17.12');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (39, 'NAVARRO', 'Timothée', '+41.22.774.40.69');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (40, 'ABEILLE', 'Aniel', '+41.22.774.09.29');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (41, 'HAEFELIN', 'Martin', '+41.22.731.88.02');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (42, 'ANELLO', 'Fabien', '+41.22.731.15.31');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (43, 'DAIBER', 'Sunny', '+41.22.300.05.69');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (44, 'GERBER', 'Diana', '+41.22.300.65.22');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (45, 'VUILLEUMIER', 'Killian', '+41.22.786.63.37');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (46, 'DUNAND', 'Victor', '+41.22.506.19.29');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (47, 'JOSS', 'Marc-Olivier', '+41.22.344.65.74');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (48, 'GRASSI', 'Defne', '+41.22.794.77.93');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (49, 'IMHOF', 'Quentin', '+41.22.732.14.07');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (50, 'BLANC', 'Harald', '+41.22.341.43.71');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (51, 'SCHNYDER', 'Florian', '+41.22.347.56.61');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (52, 'VALERI', 'Sylvain', '+41.22.757.92.08');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (53, 'DE PURY', 'Sophie', '+41.22.792.43.41');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (54, 'BOADA', 'Nuria', '+41.22.349.94.25');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (55, 'HOMINAL', 'Stéphanie', '+41.22.782.18.63');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (56, 'BOUILLAULT', 'Pierre', '+41.22.755.10.08');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (57, 'TETI', 'Frédéric', '+41.76.348.00.07');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (58, 'BOURQUARD', 'Séverine', '+41.22.348.37.34');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (59, 'HUMAIR', 'Vincent', '+41.22.635.03.57');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (60, 'VIEKE', 'Raphaël', '+41.22.733.39.76');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (61, 'BURRI', 'Anne-Capucine', '+41.22.798.52.80');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (62, 'JARGY', 'Tim', '+41.79.750.52.34');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (63, 'CERUTTI', 'Fatim', '+41.22.793.82.43');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (64, 'FLEISSNER', 'Lara', '+41.22.341.96.23');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (65, 'NJIMA', 'Estelle', '+41.22.382.41.91');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (66, 'SCHOT', 'Sophie', '+41.22.796.98.13');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (67, 'VUILLEMENOT', 'Stéphane', '+41.22.344.24.67');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (68, 'DUBUIS', 'Thibaud', '+41.22.349.81.69');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (69, 'SCHIFFER', 'Carole', '+41.22.734.02.81');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (70, 'ANDERES', 'Manon', '+41.22.345.14.61');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (71, 'VOUILLOZ', 'Maxence', '+41.22.734.25.73');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (72, 'WALTER', 'Vera', '+41.22.248.26.13');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (73, 'SONZOGNI', 'Jérôme', '+41.22.794.89.05');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (74, 'JONES', 'Aurore', '+41.22.794.10.04');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (75, 'SAUTEREL', 'Marco', '+41.79.740.43.12');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (76, 'GIARDINA', 'Olivier', '+41.22.341.43.78');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (77, 'BUCHET', 'Camille', '+41.22.756.00.12');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (78, 'PHILLOT', 'Yosuke', '+41.22.346.88.28');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (79, 'WINKILMANN', 'Camille', '+41.22.341.24.83');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (80, 'MORAL', 'Djann', '+41.22.349.50.53');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (81, 'VULLIEZ', 'Alexia', '+41.22.733.42.58');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (82, 'LOBAO', 'Quentin', '+41.22.740.51.06');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (83, 'GAVARD', 'Laetitia', '+41.22.700.24.83');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (84, 'MAURY', 'Nicolas', '+41.22.349.73.27');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (85, 'HUNZIKER', 'Léa', '+41.22.786.44.01');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (86, 'SARFATIS', 'Lucien', '+41.79.785.73.93');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (87, 'HOFMEISTER', 'Amandine', '+41.22.300.42.41');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (88, 'MAILLER', 'Aude', '+41.22.779.49.76');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (89, 'RUFENER', 'Natacha', '+41.79.349.22.45');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (90, 'FORTIS', 'Julien', '+41.22.793.07.02');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (91, 'VERNAIN', 'Meven', '+41.22.731.04.66');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (92, 'BUCHET', 'Lora', '+41.22.788.32.12');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (93, 'DEBETAZ', 'Jennifer', '+41.22.771.59.61');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (94, 'SCHWEBLIN', 'Girolamo', '+33.450.782.58.43');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (95, 'GRANDJEAN', 'Guillaume', '+41.22.792.76.92');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (96, 'ULDRY', 'Marie-Isabel', '+33.450.757.35.91');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (97, 'MORON', 'Arthur', '+41.22.793.81.01');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (98, 'VULLIEZ', 'Camille', '+41.22.733.44.18');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (99, 'PASQUIER', 'Xavier', '+41.22.757.76.22');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (100, 'PICOZZI', 'Sidoine', '+41.22.774.28.02');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (101, 'ROUX', 'Michaël', '+41.22.777.22.33');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (102, 'BRIQUE', 'Sophie', '+33.450.349.31.84');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (103, 'CUDET', 'Paul', '+41.22.784.40.61');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (104, 'SCHERLY', 'Irina', '+41.22.734.46.01');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (105, 'WICKI', 'Yann', '+41.22.774.20.00');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (106, 'ZOTTI', 'Michael', '+41.22.349.35.07');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (107, 'GUYAZ', 'Lisa', '+41.22.786.25.84');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (108, 'HOTTINGGER', 'Edouard', '+41.22.343.36.00');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (109, 'RAIS', 'Florent', '+41.22.771.76.19');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (110, 'DELEAVAL', 'Elliott', '+41.22.700.29.51');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (111, 'PRATOLINI', 'Arnaud', '+41.22.342.53.46');
INSERT INTO Participant (NoPart, Nom, Prenom, Telephone) VALUES (112, 'STOPPINI', 'Cosette', '+41.22.349.84.74');
commit;

-- Table Activite
DROP TABLE IF EXISTS Activite;
CREATE TABLE Activite
(
  NoAct   INT AUTO_INCREMENT NOT NULL COMMENT 'Numéro de l''activité (identifiant)',
  Libelle VARCHAR(255)                COMMENT 'Libellé de l''activité',
  PRIMARY KEY (NoAct)
);
INSERT INTO Activite (NoAct, Libelle) VALUES (1, 'Muséum d''histoire naturelle');
INSERT INTO Activite (NoAct, Libelle) VALUES (2, 'Musée Olympique');
INSERT INTO Activite (NoAct, Libelle) VALUES (3, 'Musée sur Roulettes');
INSERT INTO Activite (NoAct, Libelle) VALUES (4, 'Le monde de l''écrit');
INSERT INTO Activite (NoAct, Libelle) VALUES (5, 'Musée des télécommunications');
INSERT INTO Activite (NoAct, Libelle) VALUES (6, 'Musée international de la Croix-Rouge et du Croissant Rouge');
INSERT INTO Activite (NoAct, Libelle) VALUES (7, 'Aéroport international');
INSERT INTO Activite (NoAct, Libelle) VALUES (8, 'Station d''épuration d''Aïre');
INSERT INTO Activite (NoAct, Libelle) VALUES (9, 'Le Cern');
INSERT INTO Activite (NoAct, Libelle) VALUES (10, 'Les Laiteries réunies');
INSERT INTO Activite (NoAct, Libelle) VALUES (11, 'Hôpital cantonal');
INSERT INTO Activite (NoAct, Libelle) VALUES (12, 'La chocolaterie');
INSERT INTO Activite (NoAct, Libelle) VALUES (13, 'L''ONU en action');
INSERT INTO Activite (NoAct, Libelle) VALUES (14, 'Skyguide');
INSERT INTO Activite (NoAct, Libelle) VALUES (15, 'Pathé Balexert');
INSERT INTO Activite (NoAct, Libelle) VALUES (16, 'Transport handicap');
INSERT INTO Activite (NoAct, Libelle) VALUES (17, 'Les chiens de police');
INSERT INTO Activite (NoAct, Libelle) VALUES (18, 'Le Bharata Natyam');
INSERT INTO Activite (NoAct, Libelle) VALUES (19, 'La magie');
INSERT INTO Activite (NoAct, Libelle) VALUES (20, 'Atelier de décoration TSR');
INSERT INTO Activite (NoAct, Libelle) VALUES (21, 'Les archives d''Etat');
INSERT INTO Activite (NoAct, Libelle) VALUES (22, 'Les instruments de musique');
INSERT INTO Activite (NoAct, Libelle) VALUES (23, 'Le Flamenco');
INSERT INTO Activite (NoAct, Libelle) VALUES (24, 'A la découverte de l''écriture Braille');
INSERT INTO Activite (NoAct, Libelle) VALUES (25, 'L''alimentation et le sport');
INSERT INTO Activite (NoAct, Libelle) VALUES (26, 'Reporter à la Tribune de Genève');
INSERT INTO Activite (NoAct, Libelle) VALUES (27, 'Sur les traces de J.-J.-Rousseau');
INSERT INTO Activite (NoAct, Libelle) VALUES (28, 'La route des artisans d''art');
INSERT INTO Activite (NoAct, Libelle) VALUES (29, 'Initiation aux claquettes');
INSERT INTO Activite (NoAct, Libelle) VALUES (30, 'La photographie');
INSERT INTO Activite (NoAct, Libelle) VALUES (31, 'La cuisine et la pâtisserie');
INSERT INTO Activite (NoAct, Libelle) VALUES (32, 'La bijouterie');
INSERT INTO Activite (NoAct, Libelle) VALUES (33, 'Théatre et improvisation');
INSERT INTO Activite (NoAct, Libelle) VALUES (34, 'La poterie');
INSERT INTO Activite (NoAct, Libelle) VALUES (35, 'Voyage à l''intérieur du corps');
INSERT INTO Activite (NoAct, Libelle) VALUES (36, 'Météosuisse');
INSERT INTO Activite (NoAct, Libelle) VALUES (37, 'Petits animaux de nos rivières');
INSERT INTO Activite (NoAct, Libelle) VALUES (38, 'La Sauvegarde du Léman');
INSERT INTO Activite (NoAct, Libelle) VALUES (39, 'Géologie');
INSERT INTO Activite (NoAct, Libelle) VALUES (40, 'La colère des Volcans');
INSERT INTO Activite (NoAct, Libelle) VALUES (41, 'Vol captif en montgolfière');
INSERT INTO Activite (NoAct, Libelle) VALUES (42, 'La luge d''été');
INSERT INTO Activite (NoAct, Libelle) VALUES (43, 'Le cricket');
INSERT INTO Activite (NoAct, Libelle) VALUES (44, 'Le sumo');
INSERT INTO Activite (NoAct, Libelle) VALUES (45, 'Le squash');
INSERT INTO Activite (NoAct, Libelle) VALUES (46, 'Vovinam vietvodao');
INSERT INTO Activite (NoAct, Libelle) VALUES (47, 'Sur le Rhône en canoë');
INSERT INTO Activite (NoAct, Libelle) VALUES (48, 'Le jeu de quilles');
INSERT INTO Activite (NoAct, Libelle) VALUES (49, 'Le tennis de table');
INSERT INTO Activite (NoAct, Libelle) VALUES (50, 'Le rink-hockey');
INSERT INTO Activite (NoAct, Libelle) VALUES (51, 'Le baseball');
INSERT INTO Activite (NoAct, Libelle) VALUES (52, 'Le bicross');
INSERT INTO Activite (NoAct, Libelle) VALUES (53, 'Les fléchettes');
INSERT INTO Activite (NoAct, Libelle) VALUES (54, 'Le football de table');
INSERT INTO Activite (NoAct, Libelle) VALUES (55, 'Le motocross');
INSERT INTO Activite (NoAct, Libelle) VALUES (56, 'Nous et le soleil');
INSERT INTO Activite (NoAct, Libelle) VALUES (57, 'Les chauve-souris');
INSERT INTO Activite (NoAct, Libelle) VALUES (58, 'Course de lévriers');
INSERT INTO Activite (NoAct, Libelle) VALUES (59, 'La spéléologie');
INSERT INTO Activite (NoAct, Libelle) VALUES (60, 'Les joies de la navigation');
INSERT INTO Activite (NoAct, Libelle) VALUES (61, 'Initiation à la pêche');
INSERT INTO Activite (NoAct, Libelle) VALUES (62, 'L''autodéfense');
commit;

-- Table PrendPart
DROP TABLE IF EXISTS PrendPart;
CREATE TABLE PrendPart
(
  NoPart  INT NOT NULL COMMENT 'Identifiant du participant',
  NoAct   INT NOT NULL COMMENT 'Identifiant de l''activité',
  PRIMARY KEY (NoPart, NoAct),
  FOREIGN KEY (NoPart) REFERENCES Participant (NoPart),
  FOREIGN KEY (NoAct) REFERENCES Activite (NoAct)
);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 21);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (25, 22);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (19, 51);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (19, 7);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 57);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (40, 8);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 30);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (40, 61);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (54, 14);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (56, 49);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 8);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 27);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 43);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 13);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 18);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (19, 58);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 31);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 61);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (70, 18);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 36);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 26);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 16);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (56, 29);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 58);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 7);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 48);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (70, 2);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 13);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 20);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 5);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (54, 35);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (56, 48);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 23);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 49);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (70, 56);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (25, 46);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 58);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 6);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 59);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (70, 17);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (54, 36);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 54);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 18);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (19, 27);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (56, 28);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 20);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 4);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (56, 54);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (54, 53);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (19, 4);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (40, 35);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 17);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (40, 17);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 32);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (56, 9);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 42);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (54, 21);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (56, 23);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (19, 22);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 34);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 6);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 4);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 30);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (40, 46);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (21, 16);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (40, 32);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 2);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (25, 43);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 40);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (50, 60);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (40, 49);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (54, 29);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 27);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (42, 4);
INSERT INTO PrendPart (NoPart, NoAct) VALUES (18, 53);

COMMIT;