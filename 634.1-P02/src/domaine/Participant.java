/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domaine;

import java.util.ArrayList;

/**
 *
 * @author Jonathan Blum
 */
public class Participant {
    private final int num;
    private final String nom;
    private final String prenom;
    private final String telephone;
    private ArrayList activites = new ArrayList();

    public Participant(int num, String nom, String prenom, String telephone) {
        this.num = num;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
    }

    public int getNum() {
        return num;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public ArrayList getActivites() {
        return activites;
    }
    
    public Activite getActivite(int i) {
        return (Activite) activites.get(i);
    }
    
    public void addActivite(Activite a) {
        if(!activites.contains(a))
            activites.add(a);
    }
    
    public void removeActivite(Activite a) {
        activites.remove(a);
    }
    
    public void setActivites(ArrayList activites) {
        this.activites = activites;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.num;
        hash = 47 * hash + (this.nom != null ? this.nom.hashCode() : 0);
        hash = 47 * hash + (this.prenom != null ? this.prenom.hashCode() : 0);
        hash = 47 * hash + (this.telephone != null ? this.telephone.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Participant other = (Participant) obj;
        if (this.num != other.num) {
            return false;
        }
        if ((this.nom == null) ? (other.nom != null) : !this.nom.equals(other.nom)) {
            return false;
        }
        if ((this.prenom == null) ? (other.prenom != null) : !this.prenom.equals(other.prenom)) {
            return false;
        }
        if ((this.telephone == null) ? (other.telephone != null) : !this.telephone.equals(other.telephone)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nom + ' ' + this.prenom;
    }
    
    
}
