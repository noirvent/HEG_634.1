/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domaine;

/**
 *
 * @author jonathan
 */
public class Activite {
    private final int num;
    private final String libelle;

    public Activite(int num, String libelle) {
        this.num = num;
        this.libelle = libelle;
    }

    public int getNum() {
        return num;
    }

    public String getLibelle() {
        return libelle;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.num;
        hash = 37 * hash + (this.libelle != null ? this.libelle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Activite other = (Activite) obj;
        if (this.num != other.num) {
            return false;
        }
        if ((this.libelle == null) ? (other.libelle != null) : !this.libelle.equals(other.libelle)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return this.libelle;
    }  
}
