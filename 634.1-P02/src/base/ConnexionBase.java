/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package base;

import base.mysql.Outils;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Jonathan Blum
 */
public class ConnexionBase {
    private static final String NOM_BASE = "GestionAct";
    private static Connection con = null;
    
    private static void connect() {
        try {
            con = Outils.connect(NOM_BASE);
        } catch (SQLException e) {
            System.err.println("ConnexionBase.connect(): " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.err.println("ConnexionBase.connect(): " + e.getMessage());
        }         
    }
    
    public static Connection get() {
        if(con == null) connect();
        return con;
    }
    
    public static void close() {
        try {
            con.close(); 
            con = null;
        } catch (SQLException e) {
            System.err.println("ConnexionBase.close(): " + e.getMessage());
        }
    }
}
