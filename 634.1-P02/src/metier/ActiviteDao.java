/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import base.ConnexionBase;
import domaine.Activite;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author jonathan
 */
public class ActiviteDao {
    
    public static ArrayList getActivites() {
         ArrayList liste = new ArrayList();
         try {
             Connection con = ConnexionBase.get();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(
                     "SELECT `NoAct`, `Libelle` " +
                     "FROM `Activite` " +
                     "ORDER BY `Libelle`"
             );
             while (rs.next()) {
                 liste.add(new Activite(rs.getInt("NoAct"), 
                                        rs.getString("Libelle"))
                 );
             }
             stmt.close();
         } catch (SQLException e) {
             System.err.println("ActiviteDao.getActivite(): " + e.getMessage());
             return null;
         }
         
         return liste;
    }
}
