/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import base.ConnexionBase;
import domaine.Activite;
import domaine.Participant;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Jonathan Blum
 */
public class ParticipantDao {
    
    public static ArrayList getParticipants() {
         ArrayList liste = new ArrayList();
         try {
             Connection con = ConnexionBase.get();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(
                     "SELECT `NoPart`, `Nom`, `Prenom`, `Telephone` " +
                     "FROM `Participant` " +
                     "ORDER BY `Nom`"
             );
             while (rs.next()) {
                 liste.add(new Participant(rs.getInt("NoPart"), 
                                           rs.getString("Nom"),
                                           rs.getString("Prenom"),
                                           rs.getString("Telephone"))
                 );
             }
             stmt.close();
         } catch (SQLException e) {
             System.err.println("ParticipantDao.getParticipants(): " + e.getMessage());
             return null;
         }
         
         return liste;
    }
    
    public static ArrayList getParticipations(Participant p) {
         ArrayList liste = new ArrayList();
         try {
             Connection con = ConnexionBase.get();
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(
                     "SELECT `Activite`.`NoAct`, `Activite`.`Libelle`" +
                     " FROM `Activite`" +
                     " JOIN `PrendPart`" +
                     " ON `Activite`.`NoAct` = `PrendPart`.`NoAct`" +
                     " WHERE `PrendPart`.`NoPart` = " + p.getNum() + 
                     " ORDER BY `Libelle`"
             );
             while (rs.next()) {
                 liste.add(new Activite(rs.getInt("NoAct"), 
                                        rs.getString("Libelle"))
                 );
             }
             stmt.close();
         } catch (SQLException e) {
             System.err.println("ParticipantDao.getParticipants(): " + e.getMessage());
             return null;
         }
         
         return liste;
    }
    
    public static void removeParticipation(Participant p, Activite a) {
        
        try {
           Connection con = ConnexionBase.get();
           Statement stmt = con.createStatement();
           stmt.executeUpdate("DELETE FROM `PrendPart`" +
                              " WHERE NoPart = " + p.getNum() + 
                              " AND NoAct = " + a.getNum());
           stmt.close();
        } catch (SQLException e) {
            System.err.println("ParticipantDao.removeParticipation(): " + e.getMessage());
        }
    }
    
    public static void addParticipation(Participant p, Activite a) {
        try {
           Connection con = ConnexionBase.get();
           Statement stmt = con.createStatement();
           stmt.executeUpdate("INSERT INTO `PrendPart` (`NoPart`, `NoAct`)" +
                              "VALUES (" + p.getNum()+ "," + a.getNum()+ ")");
           stmt.close();
        } catch (SQLException e) {
            System.err.println("ParticipantDao.addParticipation(): " + e.getMessage());
        }
    }
}
