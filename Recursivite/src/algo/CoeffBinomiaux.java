/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo;

/**
 *
 * @author Jonathan Blum
 */
public class CoeffBinomiaux extends Recursivite {
    
    public static long recursive1(long n, long m) {
        opcount++;
        return (m==0 || n==m) ? 1 : recursive1(n-1, m-1) + recursive1(n-1, m);
    }
    
    public static long recursive2(long n, long m) {
        opcount++;
        return (m==0 || n==m) ? 1 : n * recursive2(n-1, m-1)/m;
    }   
    
    public static void main(String[] args) {
        
    }
}
