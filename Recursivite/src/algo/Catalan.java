/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo;

import tools.Timer;

/**
 *
 * @author Jonathan Blum
 */
public class Catalan extends Recursivite {
    
    public static long recursive1(int n) {
        opcount ++;
        return (n<=1) ? 1 : recursive1(n-1) * 2 * (2*n-1) / (n+1);
    }
    public static long recursive2(int n) {
        opcount ++;
        return (n<=1) ? 1 : CoeffBinomiaux.recursive2(2*n, n) / (n+1);
    }
    public static long iterative(int n) {
        opcount ++;
        if (n<=1) return 1;
        long res = 0;
        for (int i = 0; i < n; i++) {
            res += iterative(i) * iterative(n-i-1);
        }
        return res;
    }    
    
    public static void main(String[] args) {
        int n = 8;
        System.out.println("*** Catalan 1 ***");
        
        for (int i = 0; i <= n; i++) {
            startCount();
            Timer.start();
            System.out.println(String.format("Catalan n=%d: %d [%d] [%.4f]", i, Catalan.recursive1(i), getCount(), Timer.stop()));
        }   
 
        System.out.println("*** Catalan 2 ***");
        for (int i = 0; i <= n; i++) {
            startCount();
            Timer.start();
            System.out.println(String.format("Catalan n=%d: %d [%d] [%.4f]", i, Catalan.recursive2(i), getCount(), Timer.stop()));
        }  
        
        System.out.println("*** Catalan 3 ***");
        for (int i = 0; i <= n; i++) {
            startCount();
            Timer.start();
            System.out.println(String.format("Catalan n=%d : %d [%d] [%.4f]", i,  Catalan.iterative(i), getCount(), Timer.stop()));
        }         
    }
}
