/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo;

import static algo.Recursivite.startCount;
import java.util.HashMap;
import tools.Timer;

/**
 *
 * @author Jonathan Blum
 */
public class Fibonacci extends Recursivite {
    static HashMap<Integer, Long> resultHistory = new HashMap<>();

    public static long recursive(int n) {
        opcount++;
        if(n<0) throw new RuntimeException("La valeur doit être positive");
        return (n<=1)?n:recursive(n-1) + recursive(n-2);        
    }

    public static long recursive2 (int n) {
        opcount++;
        if(n<0) throw new RuntimeException("La valeur doit être positive");
        if(n<=1) return n;
        if(resultHistory.containsKey(n))
            return resultHistory.get(n);
        
        long result = recursive2(n-1) + recursive2(n-2);
        resultHistory.put(n, result);
        return result;
    }
    public static long iterative(int n) {
        opcount++;
        if(n<0) throw new RuntimeException("La valeur doit être positive");
        if(n<=1) return n;
        
        long penultimate = 0;
        long prev = 1;
        long r = 0;
        for (int i = 2; i <= n; i++) {
            opcount++;
            r = prev + penultimate;
            penultimate = prev;
            prev = r;  
        }
        return r;
    }
    
    public static void main(String[] args) {
        int n = 101;
        System.out.println("*** Fibonacci récursive ***");
        for (int i = 1; i <= 41; i+=10) {
            startCount();
            Timer.start();
            System.out.println(String.format("Fibonacci(%d): %d [%d] [%.4f]", i, Fibonacci.recursive(i), getCount(), Timer.stop()));
        }
        
        System.out.println("*** Fibonacci récursive Optimized ***");
        for (int i = 1; i <= n; i+=10) {
            startCount();
            Timer.start();
            resultHistory = new HashMap<>();
            System.out.println(String.format("Fibonacci(%d): %d [%d] [%.4f]", i, Fibonacci.recursive2(i), getCount(), Timer.stop()));
        }
        
        System.out.println("*** Fibonacci itérative ***");
        for (int i = 1; i <= n; i+=10) {
            startCount();
            Timer.start();
            System.out.println(String.format("Fibonacci(%d): %d [%d] [%.4f]", i, Fibonacci.iterative(i), getCount(), Timer.stop()));
        }        
    }
}
