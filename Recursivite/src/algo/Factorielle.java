/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo;

import tools.Timer;

/**
 *
 * @author Jonthan Blum
 */
public class Factorielle extends Recursivite {
    /**
     * Retourne x! par récursivité
     * @param x an int
     * @return the result of x!
     */
    public static long recursive(int x) {
        opcount++;
        if(x<1) throw new RuntimeException("La valeur doit être strictement positive");
        return (x>1) ? recursive(x-1)*x:x;
    }
    /**
     * Retourne x! par itération
     * @param x an int
     * @return the result of x!
     */    
    public static long iterative(long x) {
        opcount ++;
        if(x<1) throw new RuntimeException("La valeur doit être strictement positive");
        for (long i = x-1; i > 1; i--) {
            opcount ++;
            x *= i;
        }
        return x;        
    }
    
    public static void main(String[] args) {
        int n = 20;
        System.out.println("*** Factorielle récursive ***");
        for (int i = 1; i <= n; i++) {
            startCount();
            Timer.start();
            System.out.println(String.format("%d!: %d [%d] [%.4f]", i, Factorielle.recursive(i), getCount(), Timer.stop()));
        }
        System.out.println("*** Factorielle itérative ***");
        for (int i = 1; i <= n; i++) {
            startCount();
            Timer.start();
            System.out.println(String.format("%d!: %d [%d] [%.4f]", i, Factorielle.iterative(i), getCount(), Timer.stop()));
        }
    }
}
