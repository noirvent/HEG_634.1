/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo;

/**
 *
 * @author Jonathan Blum
 */
public class Hannoi extends Recursivite {
    
    public static void recursive(int disk, String src, String spr, String dst) {
        opcount++;
        if(disk == 1)
            System.out.println("De " + src + " à " + dst);
        else {
                recursive(disk-1, src, dst, spr);
                System.out.println("De " + src + " à " + dst);
                recursive(disk-1, spr, src, dst);
        }
    }   
    
    public static void main(String[] args) {
        
    }
}
