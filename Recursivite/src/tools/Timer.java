/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tools;
/**
 *
 * @author jonathan
 */
public class Timer {
    private static long  start;
    
    public static void start() {
        start = System.nanoTime();
    }
    
    public static float stop() {
        return (System.nanoTime()-start) / 1000f;
    }
}
