/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo.tp02;

/**
 *
 * @author Jonathan Blum
 */
public class A02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int a = 24157817;
        int b = 39088169;

             
        System.out.println("Euclide Récursif : " + EuclideRecursif(a, b));
        System.out.println("Euclide iteratif : " + EuclideIteratif(a, b));
        
    }
    
    public static long EuclideRecursif(int a, int b) {
        return (a==0 || b == 0) ? a+b: EuclideRecursif(b, a%b);
    }
    public static long EuclideIteratif(int a, int b) {
        
        if(a == 0 || b == 0) 
            return a+b;
        //1. Reste de A par B
        int r = a%b;
        
        //2. Remplace A par B, puis B par R, jusqu'à obtenir 0
        while(r > 0) {
            a = b;
            b = r;
            r = a%b;
        }
        
        //3. Renvoi du terme précédent de la suite (voir ligne 39)
        return b;
    }
    
}
